use std::env;
use std::fs;
use std::io::Write;
use std::path;
use std::time::{SystemTime, UNIX_EPOCH};

struct Activity {
    start: u64,
    end: Option<u64>,
    category: String,
    lean_tag: Option<char>,
}

//TODO: move into persistence module
static PERSISTENCE_FOLDER: &str = "/home/marco/rutitra/"; //TODO: make user agnostic
static CATEGORIES_FILE: &str = "categories.txt";
static ACTIVITIES_FILE: &str = "activities.txt";

fn main() {
    // check first if needed folder and file structure is given
    check_and_create_persistence_structure();
    // first argument is the binary itself
    let args: Vec<String> = env::args().skip(1).collect();
    if args.is_empty() {
        println!("missing command, try 'rutitra help' for more information");
        return;
    }

    let command = &args[0];
    let further_args = &args[1..];

    match command.as_str() {
        "cat" => handle_category_command(&further_args),
        "start" => start_activity(&further_args),
        "stop" => stop_activity(&further_args),
        "show" => show_activities(),
        "report" => report(&further_args),
        _ => print_help(),
    };
}

fn handle_category_command(args: &[String]) {
    if args.is_empty() {
        print_categories(&get_all_categories());
        return;
    }
    let subcommand = &args[0];
    match subcommand.as_str() {
        "list" => print_categories(&get_all_categories()),
        "create" | "c" => create_category(&args[1..]),
        "delete" | "d" => delete_category(&args[1..]),
        _ => print_help(),
    }
}

fn print_categories(categories: &[String]) {
    println!("Available categories:");
    for category in categories {
        println!("  {}", category);
    }
}

fn create_category(args: &[String]) {
    if args.is_empty() {
        print_help();
        return;
    }
    let category = &args[0];
    let existing_categories = get_all_categories();
    if existing_categories.contains(&category) {
        println!("category already exists!");
        return;
    }
    add_category(category);
}

fn delete_category(args: &[String]) {
    if args.is_empty() {
        print_help();
        return;
    }
    let category = &args[0];
    let existing_categories = get_all_categories();
    if !existing_categories.contains(&category) {
        println!("category does not exist!");
        return;
    }
    remove_category(category);
}

// persistence
fn check_and_create_persistence_structure() -> bool {
    // this code is not DRY, it's WET :)
    let folder_path = path::Path::new(PERSISTENCE_FOLDER);
    match fs::metadata(PERSISTENCE_FOLDER) {
        Ok(folder) => {
            if !folder.is_dir() {
                println!("Not a folder: {}", PERSISTENCE_FOLDER);
                return false;
            }
        }
        Err(_) => {
            println!(
                "Folder '{}' and relevant files do not exist. Creating them now:",
                PERSISTENCE_FOLDER
            );
            print!("  - creating folder... ");
            if let Err(err) = fs::create_dir_all(folder_path) {
                println!("error: {}", err);
                return false;
            }
            println!("ok");
        }
    };

    let activities_filename = get_activities_filename();
    match fs::metadata(&activities_filename) {
        Ok(file) => {
            if !file.is_file() {
                println!("Not a file: {}", &activities_filename);
                return false;
            }
        }
        Err(_) => {
            print!("  - creating activities file... ");
            if let Err(err) = fs::File::create(&activities_filename) {
                println!("error: {}", err);
                return false;
            }
            println!("ok");
        }
    }

    let categories_filename = get_categories_filename();
    match fs::metadata(&categories_filename) {
        Ok(file) => {
            if !file.is_file() {
                println!("Not a file: {}", &categories_filename);
                return false;
            }
        }
        Err(_) => {
            print!("  - creating categories file... ");
            if let Err(err) = fs::File::create(&categories_filename) {
                println!("error: {}", err);
                return false;
            }
            println!("ok");
        }
    }

    true
}

fn get_activities_filename() -> String {
    let mut filename = String::from(PERSISTENCE_FOLDER);
    filename.push_str(ACTIVITIES_FILE);
    filename
}

fn get_categories_filename() -> String {
    let mut filename = String::from(PERSISTENCE_FOLDER);
    filename.push_str(CATEGORIES_FILE);
    filename
}

fn get_all_categories() -> Vec<String> {
    let filename = get_categories_filename();
    fs::read_to_string(&filename)
        .unwrap()
        .lines()
        .map(|str| String::from(str))
        .collect()
}

fn add_category(category: &str) {
    if category.contains(',')
        || category.contains(' ')
        || category.contains('\t')
        || category.contains('\n')
        || category.contains('\r')
    {
        println!("Category names may not contain whitespaces or commas");
        return;
    }
    let filename = get_categories_filename();
    let mut file = fs::OpenOptions::new().append(true).open(&filename).unwrap();
    if let Err(err) = writeln!(file, "{}", category) {
        println!("Could not update categories! Error: {}", err);
    }
}

fn remove_category(category: &str) {
    // re-reading the file is not so nice, but well, here we go
    let filename = get_categories_filename();
    let updated_content: String = fs::read_to_string(&filename)
        .unwrap()
        .lines()
        .filter(|line| !line.eq(&category))
        .collect::<Vec<&str>>()
        .join("\n");
    println!("writing: {}", updated_content);
    let file = fs::OpenOptions::new()
        .write(true)
        .truncate(true)
        .open(&filename)
        .unwrap();
    if let Err(err) = writeln!(&file, "{}", &updated_content) {
        println!("Could not update categories! Error: {}", err);
    }
}

fn add_activity(activity: &Activity) {
    //TODO: could probably be implemented with a formatting trait
    let line = serialise_activity(&activity);
    let filename = get_activities_filename();
    let mut file = fs::OpenOptions::new().append(true).open(&filename).unwrap();
    if let Err(err) = writeln!(file, "{}", &line) {
        println!("Could not add activity! Error: {}", err);
    }
}

fn rewrite_activities(activities: &[&Activity]) {
    //TODO: could probably be implemented with a formatting trait
    let content = activities
        .iter()
        .map(|a| serialise_activity(&a))
        .collect::<Vec<String>>()
        .join("\n");

    let filename = get_activities_filename();
    let file = fs::OpenOptions::new()
        .write(true)
        .truncate(true)
        .open(&filename)
        .unwrap();
    if let Err(err) = writeln!(&file, "{}", &content) {
        println!("Could not add activity! Error: {}", err);
    }
}

//TODO: implement or use trait
fn serialise_activity(activity: &Activity) -> String {
    let mut line = String::new();
    line.push_str(&activity.start.to_string());
    line.push(',');
    match &activity.end {
        Some(end) => line.push_str(&end.to_string()),
        None => (),
    }
    line.push(',');
    line.push_str(&activity.category);
    line.push(',');
    match &activity.lean_tag {
        Some(tag) => line.push(*tag),
        None => (),
    }
    line
}

fn deserialise_activity(line: &str) -> Activity {
    let mut arr = line.split(',');
    Activity {
        start: match arr.next() {
            Some(start_str) => start_str.parse::<u64>().unwrap(),
            None => panic!("error reading start attribute from activity"),
        },
        end: match arr.next() {
            Some(end_str) => {
                if end_str.is_empty() {
                    Option::None
                } else {
                    Option::from(end_str.parse::<u64>().unwrap())
                }
            }
            None => panic!("error reading end attribute from activity"),
        },
        category: match arr.next() {
            Some(cat_str) => cat_str.to_string(),
            None => panic!("error reading category attribute from activity"),
        },
        lean_tag: match arr.next() {
            Some(tag) => {
                if tag.is_empty() {
                    Option::None
                } else {
                    tag.chars().next()
                }
            }
            None => None,
        },
    }
}

fn get_all_activities() -> Vec<(usize, Activity)> {
    let filename = get_activities_filename();
    fs::read_to_string(filename)
        .unwrap()
        .lines()
        .enumerate()
        .filter(|(_, line)| !line.is_empty())
        .map(|(n, line)| (n, deserialise_activity(&line)))
        .collect()
}

fn get_ongoing_activities() -> Vec<(usize, Activity)> {
    get_all_activities()
        .into_iter()
        .filter(|(_, activity)| activity.end.is_none())
        .collect()
}

fn get_finished_activities_between(since: u64, until: u64) -> Vec<(usize, Activity)> {
    get_all_activities()
        .into_iter()
        .filter(|(_, activity)| activity.start >= since)
        .filter(|(_, activity)| match activity.end {
            Some(end) => end <= until,
            None => false,
        })
        .collect()
}

fn update_activity(id: usize, activity: &Activity) {
    let old_activities = get_all_activities();
    let activities: Vec<&Activity> = old_activities
        .iter()
        .map(|(n, a)| if id == *n { activity } else { a }) //replace the activity with the matching line number
        .collect();
    rewrite_activities(&activities);
}
//end persistence

fn start_activity(args: &[String]) {
    if args.is_empty() {
        print_help();
        return;
    }

    let category = &args[0];
    let available_categories = get_all_categories();
    if !available_categories.contains(&category) {
        println!("Selected category does not exist!");
        return;
    }

    //TODO: check if same category has ongoing activity

    let now = current_time();

    let start = if args.len() == 2 {
        let backdating = &args[1];
        match parse_backdating(&backdating) {
            Some(value) => now - value,
            None => now,
        }
    } else {
        now
    };

    let activity = Activity {
        start: start,
        end: Option::None,
        category: category.to_string(),
        lean_tag: Option::None,
    };

    add_activity(&activity);
}

fn stop_activity(args: &[String]) {
    if args.is_empty() || args.len() < 2 {
        print_help();
        return;
    }

    let category = &args[0];
    let lean_tag = &args[1];

    let now = current_time();

    let end = if args.len() == 3 {
        let backdating = &args[2];
        match parse_backdating(&backdating) {
            Some(value) => now - value,
            None => now,
        }
    } else {
        now
    };

    let lean_tag = match determine_lean_tag(&lean_tag) {
        Some(tag) => tag,
        None => {
            println!("No valid lean-tag provided. Use value-add, neutral or waste. Short forms are available ('w' for waste, 'v' or 'a' for value-add, 'n' for neutral).");
            return;
        }
    };

    let ongoing_activities = get_ongoing_activities();
    let (n, activity) = match ongoing_activities
        .iter()
        .find(|(_, activity)| activity.category.eq(category))
    {
        Some(entry) => entry,
        None => {
            println!("Did not find an ongoing activity of that category.");
            return;
        }
    };
    if end < activity.start {
        println!("End can not be after start.");
        return;
    }

    update_activity(
        *n,
        &Activity {
            start: activity.start,
            end: Option::from(end),
            category: activity.category.clone(),
            lean_tag: Option::from(lean_tag),
        },
    );
}

fn determine_lean_tag(tag: &str) -> Option<char> {
    let tag = tag.to_lowercase();
    if vec!["a", "add", "v", "value", "ad", "value-add", "add-value"].contains(&tag.as_str()) {
        return Option::from('v');
    }
    if vec!["w", "waste"].contains(&tag.as_str()) {
        return Option::from('w');
    }
    if vec!["n", "neutral"].contains(&tag.as_str()) {
        return Option::from('n');
    }
    None
}

fn show_activities() {
    println!("Currently ongoing:");
    let now = current_time();
    get_ongoing_activities().iter().for_each(|(n, activity)| {
        println!(
            "  - {} (since {}) (entry # {})",
            activity.category,
            format_duration(now - activity.start),
            n
        )
    });
}

fn format_duration(seconds: u64) -> String {
    if seconds < 60 {
        format!("{} s", seconds)
    } else if seconds < 60 * 60 {
        format!("{}:{:0>2} min", seconds / 60, seconds % 60)
    } else if seconds < 60 * 60 * 24 {
        format_duration_hours(seconds)
    } else {
        let days = seconds / (60 * 60 * 24);
        format!(
            "{} d, {}",
            days,
            &format_duration_hours(seconds % (60 * 60 * 24 * days))
        )
    }
}

fn format_duration_hours(seconds: u64) -> String {
    let hours = seconds / (60 * 60);
    let seconds_left = seconds % (60 * 60);
    let minutes = seconds_left / 60;
    format!("{}:{:0>2}:{:0>2} h", hours, minutes, seconds_left % 60)
}

fn print_help() {
    println!("Time tracker utility");
    println!("");
    println!("USAGE:");
    println!("    rutitra [COMMAND] [SUBCOMMAND] [OPTIONS]");
    println!("");
    println!("COMMANDS:");
    println!("    help                                          Prints this page");
    println!("    cat [list]                                    List available categories");
    println!("    cat create <category>                         Create new category");
    println!("    cat delete <category>                         Delete existing category");
    println!("    start <category> [backdating]                 Start activity with category, optionally x seconds ago");
    println!("    stop <category> <lean-tag> [backdating]       End activity with category and lean-tag, optionally x seconds ago");
    println!("    show                                          Show currently ongoing activities");
    println!("    report [backdating-from] [backdating-until]   Print a report, optionally providing the last x - y seconds to consider");
    println!("");
    println!("OPTION VALUES:");
    println!("    <category>    Name/id of the new category. No whitespaces or commas.");
    println!("                  [^ ,\\t\\n\\r]+");
    println!("    <lean-tag>    One of the lean-tags, with several abbreviation possibilities: value-add, neutral, waste");
    println!("                  (value-add|v|va|add-value|a|av|ad|add|waste|w|neutral|n)");
    println!("    [backdating]  Duration relative from the current time, either seconds, or an expression with time-units.");
    println!("                  [0-9]+d?([0-9]+h)?([0-9]+min)([0-9]+s)");
    println!("                  e.g.: 3600, 13d4h28min10s, 4h2s, 1d1m");
    println!("");
}

fn report(args: &[String]) {
    let now = current_time();
    let (since, until) = match args.len() {
        0 => (now - (60 * 60 * 12), now),
        1 | 2 => (
            match parse_backdating(&args[0]) {
                Some(offset) => now - offset,
                None => {
                    println!("Could not parse [backdating-since] attribute.");
                    return;
                }
            },
            if args.len() == 2 {
                match parse_backdating(&args[1]) {
                    Some(offset) => now - offset,
                    None => {
                        println!("Could not parse [backdating-since] attribute.");
                        return;
                    }
                }
            } else {
                now
            },
        ),
        _ => {
            print_help();
            return;
        }
    };

    let activities = get_finished_activities_between(since, until);
    let mut duration_total = 0;
    let mut duration_categories = std::collections::HashMap::new();
    for (_, a) in &activities {
        let duration = a.end.unwrap() - a.start;
        duration_total += duration;
        let durations_category = match duration_categories.get(&a.category) {
            Some(durations) => durations,
            None => &(0, 0, 0, 0),
        };
        let mut updated_durations = durations_category.clone();
        updated_durations.0 += duration;
        match a.lean_tag.unwrap() {
            'v' => updated_durations.1 += duration,
            'n' => updated_durations.2 += duration,
            'w' => updated_durations.3 += duration,
            x => {
                println!("Illegal lean-tag found: {}", x);
                return;
            }
        }
        duration_categories.insert(&a.category, updated_durations);
    }

    println!(
        "Report for {}{}",
        if now == until { "last " } else { "" },
        format_duration(until - since)
    );
    println!("");
    println!(
        "Total recorded duration: {}",
        format_duration(duration_total)
    );
    println!("Durations by category:");
    let mut category_report: Vec<(&&String, &(u64, u64, u64, u64))> =
        duration_categories.iter().collect();
    category_report.sort_by(|a, b| b.1 .0.cmp(&a.1 .0)); // now it gets confusing :D (order by total duration for category)

    let longest_cat_name_length = match category_report.iter().map(|x| x.0.len()).max() {
        Some(max) => max,
        None => 0,
    };

    //TODO: output in nice tables
    for cr in &category_report {
        println!(
            "  - {:width$} - {} ({:3}%) [v: {:3}%, n: {:3}%, w: {:3}%]",
            cr.0, //category
            format_duration(cr.1 .0),
            cr.1 .0 * 100 / duration_total,
            cr.1 .1 * 100 / cr.1 .0,
            cr.1 .2 * 100 / cr.1 .0,
            cr.1 .3 * 100 / cr.1 .0,
            width = longest_cat_name_length
        );
    }

    println!("");
    println!("Durations by lean-tag:");
    match category_report.iter().map(|x| *x.1).reduce(|acc, value| {
        (
            acc.0 + value.0,
            acc.1 + value.1,
            acc.2 + value.2,
            acc.3 + value.3,
        )
    }) {
        Some(x) => {
            println!(
                "  - value-add: {} ({}%)",
                format_duration(x.1),
                x.1 * 100 / x.0
            );
            println!(
                "  - neutral:   {} ({}%)",
                format_duration(x.2),
                x.2 * 100 / x.0
            );
            println!(
                "  - waste:     {} ({}%)",
                format_duration(x.3),
                x.3 * 100 / x.0
            );
        }
        None => (),
    }
}

fn parse_backdating(arg: &str) -> Option<u64> {
    //TODO: currently straight seconds only, implement something like "12h" or "1h30min5s" (see help description)
    match arg.parse::<u64>() {
        Ok(seconds) => Option::from(seconds),
        Err(_) => None,
    }
}

fn current_time() -> u64 {
    match SystemTime::now().duration_since(UNIX_EPOCH) {
        Ok(timestamp) => timestamp.as_secs(),
        Err(err) => panic!("unable to get current time! Error: {}", err),
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn test_format_duration() {
        //seconds only
        assert_eq!("5 s", format_duration(5));
        assert_eq!("59 s", format_duration(59));
        assert_eq!("0 s", format_duration(0));
        //minutes
        assert_eq!("1:05 min", format_duration(65));
        assert_eq!("1:59 min", format_duration(119));
        assert_eq!("1:00 min", format_duration(60));
        assert_eq!("10:00 min", format_duration(600));
        assert_eq!("59:59 min", format_duration(60 * 60 - 1));
        //hours
        assert_eq!("1:00:05 h", format_duration(60 * 60 + 5));
        assert_eq!("1:00:59 h", format_duration(60 * 60 + 59));
        assert_eq!("3:00:00 h", format_duration(60 * 60 * 3));
        assert_eq!("14:00:01 h", format_duration(60 * 60 * 14 + 1));
        assert_eq!("23:59:59 h", format_duration(60 * 60 * 24 - 1));
        //days
        assert_eq!("1 d, 0:00:00 h", format_duration(60 * 60 * 24));
        assert_eq!("3 d, 0:00:00 h", format_duration(60 * 60 * 24 * 3));
        assert_eq!("1000 d, 0:00:00 h", format_duration(60 * 60 * 24 * 1000));
        assert_eq!("1 d, 0:59:59 h", format_duration(60 * 60 * 25 - 1));
        assert_eq!("1 d, 23:59:59 h", format_duration(60 * 60 * 24 * 2 - 1));
    }
}
