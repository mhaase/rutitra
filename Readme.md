# rutitra

time tracker (in rust; using as few dependencies as possible)

A command line application to track your time and categorise it, providing reports afterwards.

Inspired by the practices of lean management, this tool can give overviews about time spent in (value-add|neutral|waste).

## install

`cargo install --path .`

## requirements

- [x] save data in local files
  - [x] consider if human or machine readable
- [x] categories management
- [x] start and end activities
  - [x] allow overlapping activities
  - [ ] optional: allow comments for activities
  - [x] allow providing lean-tag on end (e.g. value-add, neutral, waste)
- [x] start and end activities in the past
  - [ ] support more human writable durations
- [x] overview about current activities
- [x] reports
  - [ ] ~current week as standard~
  - [x] last 12h as standard (working day)
  - [x] for given time period
  - [x] textual report for time spent in categories
    - [x] time per category
    - [x] time per lean-tag
    - [x] time per category per lean-tag
  - [ ] optional: add graphical overview
  - [x] optional: nice formatting
- [ ] use colours :)

## commands

- [x] help
- [x] show
- [x] cat create (category)
- [x] cat [list]
- [x] cat delete (category)
- [x] start (category) [backdating]
- [x] stop (category) (lean-tag) [backdating]
- [x] report [interval]
